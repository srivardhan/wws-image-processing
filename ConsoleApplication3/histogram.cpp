#include <opencv2/opencv.hpp>
#include<iostream>

using namespace cv;
using namespace std;

int magnify = 1;

void thresh_callback(int , void *) {
	int i = 0, j = 0, total = 0, max = 0;
	int A[256] = { 0,0 };
	Mat var = imread("img2.png", 0);
	for (i = 0; i < var.rows; i++) {
		for (j = 0; j < var.cols; j++) {
			A[var.at<uchar>(i, j)]++;
		}
	}
	for (i = 0; i < 256; i++) {
		if (max < A[i]) {
			max = A[i];
		}
	}

	Mat graph = Mat(max+50, 256, CV_8UC1, Scalar(0));

	
	for (i = 0; i < 256; i++) {
		cout << i << endl;
		for (j = max+49; j > max - A[i]+49; j--) {
			graph.at<uchar>(j, i) = 255;
		}
	}
	imshow("graph", graph);
	waitKey(0);
}

int main() {
	namedWindow("graph", WINDOW_AUTOSIZE);
	createTrackbar("magnification", "graph", &magnify, 5, thresh_callback);
	thresh_callback(0, 0);
	return 0;
}