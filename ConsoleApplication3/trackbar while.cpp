#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;
int main()
{
	int a = 150, i, j, grey;
	Mat var1 = imread("img.jpg", 0);
	Mat var2(var1.rows, var1.cols, CV_8UC1, Scalar(0));
	namedWindow("trackbar", WINDOW_AUTOSIZE);
	createTrackbar("threshold", "trackbar", &a, 255);
	while (1) {
		for (i = 0; i < var1.rows; i++) {
			for (j = 0; j < var1.cols; j++) {
				if (var1.at<uchar>(i, j) <= a) {
					var2.at<uchar>(i, j) = 0;
				}
				else {
					var2.at<uchar>(i, j) = 255;
				}

			}
		}
		namedWindow("image", WINDOW_AUTOSIZE);
		imshow("image", var2);
		waitKey(2);
	}
	return 0;
}