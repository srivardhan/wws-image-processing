#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int k[3][3] = { -1,0,1,-2,0,2,-1,0,1 };
int l[3][3] = { 1,2,1,0,0,0,-1,-2,-1 };
int i, j, p, q, b = 0, b1 = 0, a = 100, b2;
Mat var1 = imread("img.jpg", 0);

void thresh_callback(int, void*){
	Mat var2(var1.rows, var1.cols, CV_8UC1, Scalar(0));
	for (i = 1; i < var1.rows - 1; i++){
		for (j = 1; j < var1.cols - 1; j++){
			for (p = i - 1; p <= i + 1; p++){
				for (q = j - 1; q <= j + 1; q++){
					b = b + (var1.at<uchar>(p, q))*k[(p - i + 1)][(q - j + 1)];
					b1 = b1 + (var1.at<uchar>(p, q))*l[(p - i + 1)][(q - j + 1)];
				}
			}
			b2 = sqrt(b*b + b1*b1);
			b = 0; b1 = 0;
			if (b2 > a){
				var2.at<uchar>(i, j) = 255;
			}
		}
	}
	imshow("image", var2);
	waitKey(0);
}

int main()
{
	namedWindow("trackbar", WINDOW_AUTOSIZE);
	createTrackbar("threshold", "trackbar", &a, 255, thresh_callback);
	thresh_callback(0, 0);
	return 0;
}