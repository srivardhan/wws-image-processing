#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
int main() {
	int i, j, b, r, g, p, q, grey, t = 150;
	Mat var1 = imread("try.png", 1);
	Mat var2(var1.rows, var1.cols, CV_8UC1, Scalar(0));
	Mat var3(var1.rows, var1.cols, CV_8UC1, Scalar(0));

	for (i = 0; i < var1.rows; i++){
		for (j = 0; j < var1.cols; j++){
			b = var1.at<Vec3b>(i, j)[0];
			g = var1.at<Vec3b>(i, j)[1];
			r = var1.at<Vec3b>(i, j)[2];
			grey = (b + g + r) / 3;
			if (grey <= t)
				(var2.at<uchar>(i, j)) = 0;
			else
				(var2.at<uchar>(i, j)) = 255;
		}
	}


	for (i = 1; i < var1.rows - 1; i++){
		for (j = 1; j < var1.cols - 1; j++){
			int f = 0;
			for (p = i - 1; p < i + 2; p++){
				for (q = j - 1; q < j + 2; q++){
					if (var2.at<uchar>(p, q) == 255){
						f = 1;
					}

				}
			}
			if (f == 1)
				var3.at<uchar>(i, j) = 255;
			else
				var3.at<uchar>(i, j) = 0;
		}
	}
	namedWindow("windows1", WINDOW_AUTOSIZE);
	imshow("windows1", var3);
	waitKey(0);
	return 0;
}