#include <opencv2/opencv.hpp>
#include<iostream>

using namespace cv;
using namespace std;

int main() {
	Mat var = imread("img.jpg", 1);
	Mat img = Mat(var.rows, var.cols, CV_8UC1, Scalar(0));
	for (int i = 0; i<var.rows; i++) {
		for (int j = 0; j<var.cols; j++) {
			int a = var.at<Vec3b>(i, j)[0];
			int b = var.at<Vec3b>(i, j)[1];
			int c = var.at<Vec3b>(i, j)[2];
			img.at<uchar>(i, j) = 0.3*a + 0.59*b + 0.11*c;
		}
	}
	imshow("geyscale", img);
	waitKey(0);
	return 0;

}