#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(){
	Mat a= Mat(8, 8, CV_8UC3, Scalar(0, 0, 0));
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
					if ((i+j)%2==0) {
						a.at<Vec3b>(i, j)[0] = 255;
						a.at<Vec3b>(i, j)[1] = 255;
						a.at<Vec3b>(i, j)[2] = 255;
					}
		}
	}
	namedWindow("abc", WINDOW_AUTOSIZE);
	imshow("abc", a);
	waitKey(0);
	return 0;
}
