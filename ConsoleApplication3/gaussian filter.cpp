#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main(){
	Mat var1 = imread("img.jpg", 0);
	Mat var2 = imread("img.jpg", 0);
	int k[3][3] = { 1,2,1,2,4,2,1,2,1 };
	int i = 0, j = 0, sum = 0, count = 0, p = 0, q = 0;

	for (i = 0; i<var2.rows - 1; i++){
		for (j = 0; j<var2.cols - 1; j++){
			for (p = i - 1; p <= i + 1; p++){
				for (q = j - 1; q <= j + 1; q++){
					if (p >= 0 && q >= 0){
						sum = sum + ((var2.at<uchar>(p, q))* k[(p - i + 1)][(q - j + 1)]);
						count = count + (k[(p - i + 1)][(q - j + 1)]);
					}
				}
			}
			var2.at<uchar>(i, j) = sum / count;
			sum = 0;
			count = 0;
		}
	}
	imshow("original", var1);
	imshow("guassianfilter", var2);
	waitKey(0);
	return 0;
}