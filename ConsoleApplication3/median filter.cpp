#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(){
	Mat var1 = imread("img.jpg", 0);
	int k[3][3] = { 1,1,1,1,1,1,1,1,1 };
	int a[9];
	int i, j, l, m, temp;
	Mat var2 = var1;
	Mat var3 = var1;

	for (i = 1; i < var1.rows - 1; i++){
		for (j = 1; j < var1.cols - 1; j++){
			a[0] = var2.at<uchar>(i, j); a[1] = var2.at<uchar>(i - 1, j); a[2] = var2.at<uchar>(i, j - 1); a[3] = var2.at<uchar>(i - 1, j - 1); a[4] = var2.at<uchar>(i + 1, j); a[5] = var2.at<uchar>(i, j + 1); a[6] = var2.at<uchar>(i + 1, j + 1); a[7] = var2.at<uchar>(i - 1, j + 1); a[8] = var2.at<uchar>(i + 1, j - 1);
			for (l = 1; l<9; ++l){
				for (m = 0; m < 8; ++m) {
					if (a[m] > a[m + 1]) {
						temp = a[m];
						a[m] = a[m + 1];
						a[m + 1] = temp;
					}
				}
			}
				
			var2.at<uchar>(i, j) = a[4];
		}
	}
	for (i = 1; i < var1.rows - 1; i++){
		for (j = 1; j < var1.cols - 1; j++){
			a[0] = var1.at<uchar>(i, j); a[1] = var1.at<uchar>(i - 1, j); a[2] = var1.at<uchar>(i, j - 1); a[3] = var1.at<uchar>(i - 1, j - 1); a[4] = var1.at<uchar>(i + 1, j); a[5] = var1.at<uchar>(i, j + 1); a[6] = var1.at<uchar>(i + 1, j + 1); a[7] = var1.at<uchar>(i - 1, j + 1); a[8] = var1.at<uchar>(i + 1, j - 1);
			for (l = 1; l < 9; ++l) {
				for (m = 0; m < 8; ++m) {
					if (a[m] > a[m + 1]) {
						temp = a[m];
						a[m] = a[m + 1];
						a[m + 1] = temp;
					}
				}
			}
			var3.at<uchar>(i, j) = a[4];
		}
	}
	
	namedWindow("original", WINDOW_AUTOSIZE);
	imshow("original", var1);
	namedWindow("median_filter_updated", WINDOW_AUTOSIZE);
	imshow("median_filter_updated", var2);
	namedWindow("median_filter", WINDOW_AUTOSIZE);
	imshow("median_filter", var3);
	waitKey(0);
	return 0;
}