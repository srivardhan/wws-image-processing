#include "opencv2/opencv.hpp"

using namespace cv;
using namespace std;

int main(int, char)
{
	Mat var = imread("img.jpg", 1);
	namedWindow("old", WINDOW_NORMAL);
	imshow("old", var);
	waitKey(0);
	for (int i = 0; i < 500; i++) {
		for (int j = 0; j < 500; j++) {
			for (int k = 0; k < 3; k++) {
				if (var.at<Vec3b>(i, j)[k] + 50 < 255) {
					var.at<Vec3b>(i, j)[k] = var.at<Vec3b>(i, j)[k] + 50;
				}
			}
		}
	}
	namedWindow("new", WINDOW_NORMAL);
	imshow("new", var);
	waitKey(0);
	return 0;
}