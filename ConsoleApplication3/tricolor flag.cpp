#include "opencv2/opencv.hpp"

using namespace cv;
using namespace std;

int main(int, char)
{
	Mat var = Mat(50, 50, CV_8UC3, Scalar(0, 0, 0));
	for (int i = 0; i < var.rows; i++) {
		for (int j = 0; j < var.cols; j++) {
			if (i <= var.rows / 3) {
				var.at<Vec3b>(i, j)[0] = 51;
				var.at<Vec3b>(i, j)[1] = 153;
				var.at<Vec3b>(i, j)[2] = 255;
			}
			else if (i <= 2 * var.rows / 3) {
				var.at<Vec3b>(i, j)[0] = 255;
				var.at<Vec3b>(i, j)[1] = 255;
				var.at<Vec3b>(i, j)[2] = 255;
			}
			else {
				var.at<Vec3b>(i, j)[0] = 8;
				var.at<Vec3b>(i, j)[1] = 136;
				var.at<Vec3b>(i, j)[2] = 19;
			}
		}
	}
	namedWindow("shanmukh", WINDOW_NORMAL);
	imshow("shanmukh", var);
	waitKey(0);
	return 0;
}