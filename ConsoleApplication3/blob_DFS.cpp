#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include<stack>
#include<iostream>
using namespace std;
using namespace cv;

int main(){
	int i, j, countb = 0, count = 0;
	Mat var1 = imread("img2.png", 0);
	Mat visited(var1.rows, var1.cols, CV_8UC1, Scalar(0));
	stack<Point> s;
	Point u;
	for (i = 0; i < var1.rows; i++){
		for (j = 0; j < var1.cols; j++){
			count++;
			if ((visited.at<uchar>(i, j) == 0) && var1.at<uchar>(i, j) >150){
				s.push(Point(i, j));
				visited.at<uchar>(i, j) = 255;
				countb++;
				while (!s.empty()){
					u = s.top();
					s.pop();
					for (int p = (int)(u.x) - 1; p <= (int)(u.x) + 1; p++){
						for (int q = (int)(u.y) - 1; q <= (int)(u.y) + 1; q++){
							if (p < 0 || q < 0 || p >= var1.rows || q <= var1.cols) {
								continue;
							}
							if (visited.at<uchar>(p, q) == 0 && var1.at<uchar>(p, q) >150){
								visited.at<uchar>(p, q) = 255;
								s.push(Point2d(p, q));
							}
						}
					}
				}
			}

		}
	}

	cout << countb << " " << endl;
	imshow("Image", var1);
	imshow("Blobs", visited);
	waitKey(0);
	return 0;
}