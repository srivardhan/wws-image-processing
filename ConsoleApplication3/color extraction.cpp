#include <opencv2/opencv.hpp>

#include<iostream>

using namespace cv;
using namespace std;

int r_low=0,r_high=255,g_low=0,g_high=255,b_low=0,b_high=255;

void thresh_callback(int p,void* q){
	int i=0,j=0,r=0,b=0,g=0;
	Mat var;
	var=imread("img.jpg",1);
	for(i=0;i<var.rows;i++){
		for(j=0;j<var.cols;j++){
			b=var.at<Vec3b>(i,j)[0];
			g=var.at<Vec3b>(i,j)[1];
			r=var.at<Vec3b>(i,j)[2];
			if(!(r>r_low && r<=r_high && b>b_low && b<=b_high && g>g_low && g<=g_high)){
				var.at<Vec3b>(i,j)[0]=0;
				var.at<Vec3b>(i,j)[1]=0;
				var.at<Vec3b>(i,j)[2]=0;
			}	
		}
	}
	imshow("window",var);
	waitKey(0);
}

int main(){
	namedWindow("window",WINDOW_AUTOSIZE);	
	createTrackbar("r_low","window",&r_low,255,thresh_callback);
	createTrackbar("r_high","window",&r_high,255,thresh_callback);
	createTrackbar("g_low","window",&g_low,255,thresh_callback);
	createTrackbar("g_high","window",&r_high,255,thresh_callback);
	createTrackbar("b_low","window",&b_low,255,thresh_callback);
	createTrackbar("b_high","window",&r_high,255,thresh_callback);
	thresh_callback(0,0);
	return 0;
}