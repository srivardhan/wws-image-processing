#include <opencv2/opencv.hpp>
#include<iostream>

using namespace cv;
using namespace std;

int thresold = 100;

void thresh_callback(int p, void* q) {
	int i = 0, j = 0;
	Mat var;
	var = imread("img.jpg", 0);
	for (i = 0; i<var.rows; i++) {
		for (j = 0; j<var.cols; j++) {
			if ((var.at<uchar>(i, j))<thresold) {
				var.at<uchar>(i, j) = 0;
			}
			else {
				var.at<uchar>(i, j) = 255;
			}
		}
	}
	imshow("new", var);
	waitKey(0);
}
int main() {
	namedWindow("Trackbars", WINDOW_AUTOSIZE);
	createTrackbar("barname", "Trackbars", &thresold, 255, thresh_callback);
	thresh_callback(0, 0);
	return 0;

}