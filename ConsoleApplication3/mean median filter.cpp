#include <opencv2/opencv.hpp>
#include<iostream>

using namespace cv;
using namespace std;

int main() {

	Mat var = imread("img.jpg", 0);
	int i = 0, j = 0, k = 0, sum1 = 0, sum2 = 0, r = 0, s = 0;
	int A[9], B[9];
	Mat meanorig = var;
	Mat meanupd = var;
	Mat tempfix = var;
	Mat medianupd = var;
	Mat medianorig = var;

	for (i = 1; i < var.rows - 1; i++) {
		for (j = 1; j < var.cols - 1; j++) {
			for (r = i - 1; r < i + 2; r++) {
				for (s = j - 1; s < j + 2; s++) {
					A[k] = var.at<uchar>(r, s);
					B[k] = tempfix.at<uchar>(r, s);
					k++;
					sum1 += var.at<uchar>(r, s);
					sum2 += tempfix.at<uchar>(r, s);
				}
			}
			sort(A, A + 9);
			sort(B, B + 9);
			medianupd.at<uchar>(i, j) = A[5];
			medianorig.at<uchar>(i, j) = B[5];
			var.at<uchar>(i, j) = sum1 / 9;
			meanorig.at<uchar>(i, j) = sum2 / 9;
			sum1 = 0;
			sum2 = 0;
			k = 0;
		}
	}
	
	meanupd = var;

	imshow("median_updated", medianupd);
	imshow("median_original", medianorig);
	imshow("mean_original", meanorig);
	imshow("mean_updated", meanupd);
	waitKey(0);
	return 0;
}