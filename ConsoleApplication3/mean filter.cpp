#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main() {
	Mat var1 = imread("img.jpg", 0);
	int k[3][3] = { 1,1,1,1,1,1,1,1,1 };
	int i, j, p, q, sum, count;
	Mat var2 = var1;
	Mat var3=var1;

	for (i = 1; i < var1.rows - 1; i++){
		for (j = 1; j < var1.cols - 1; j++){
			for (p = i - 1; p <= i + 1; p++){
				for (q = j - 1; q <= j + 1; q++){
					if (p >= 0 && q >= 0) {
						sum = sum + ((var2.at<uchar>(p, q))* k[(p - i + 1)][(q - j + 1)]);
						count = count + (k[(p - i + 1)][(q - j + 1)]);
					}
				}
			}
			var2.at<uchar>(i, j) = sum / count;
			sum = 0;
			count = 0;
		}
	}
	for (i = 1; i < var1.rows - 1; i++){
		for (j = 1; j < var1.cols - 1; j++){
			var2.at<uchar>(i, j) = (var1.at<uchar>(i, j) + var1.at<uchar>(i - 1, j) + var1.at<uchar>(i, j - 1) + var1.at<uchar>(i - 1, j - 1) + var1.at<uchar>(i + 1, j) + var1.at<uchar>(i, j + 1) + var1.at<uchar>(i + 1, j + 1) + var1.at<uchar>(i - 1, j + 1) + var1.at<uchar>(i + 1, j - 1)) / 9;
		}
	}
	namedWindow("original", WINDOW_AUTOSIZE);
	imshow("original", var1);
	namedWindow("mean_filter_updated", WINDOW_AUTOSIZE);
	imshow("mean_filter_updated", var2);
	namedWindow("mean_filter", WINDOW_AUTOSIZE);
	imshow("mean_filter", var3);
	waitKey(0);
	return 0;
}